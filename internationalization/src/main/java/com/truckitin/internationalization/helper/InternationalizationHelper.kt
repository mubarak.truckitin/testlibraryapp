package com.truckitin.internationalization.helper

import com.truckitin.internationalization.data.InternationalizationRepository
import com.truckitin.internationalization.data.db.entities.InternationalizationEntity
import com.truckitin.preferences.LanguagePreferences
import com.truckitin.preferences.Urdu
import kotlinx.coroutines.runBlocking
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InternationalizationHelper @Inject constructor(
    private val languagePreferences: LanguagePreferences,
    private val repository: InternationalizationRepository
) {
    private var listData = emptyList<InternationalizationEntity>()

    init {
        runBlocking {
            listData = repository.getListOfLocale()
        }
    }

    private fun isLocaleUrdu(): Boolean = languagePreferences.appLang == Urdu.code

    fun translate(key: String?): String {
        if (key.isNullOrEmpty()) return ""

        val item = listData.find { it.key.trim().uppercase() == key.uppercase() }
        return item?.let {
            if (isLocaleUrdu()) it.ur else it.key
        } ?: key
    }

    fun translateForEnums(enumKey: String?): String {
        if (enumKey.isNullOrEmpty()) return ""

        val item = listData.find { it.key.trim().uppercase() == enumKey.uppercase() }
        return item?.let {
            if (isLocaleUrdu()) it.ur else it.eng ?: it.key
        } ?: enumKey
    }

    fun isEnglishTranslationAvailable(): Boolean {
        return listData.any {
            return it.eng.isNullOrBlank().not()
        }
    }

    fun refreshData() {
        runBlocking {
            listData = repository.getListOfLocale()
        }
    }
}