package com.truckitin.internationalization.di

import android.content.Context
import com.truckitin.internationalization.data.db.TruckerDatabase
import com.truckitin.internationalization.utils.Constants.INTERNATIONALIZATION_DAO
import com.truckitin.internationalization.utils.Constants.TIN_PARTNER_DATABASE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object InternalizationModule{

    @Singleton
    @Provides
    @Named(TIN_PARTNER_DATABASE)
    fun provideDatabase(
        @ApplicationContext context: Context
    ): TruckerDatabase {
        return TruckerDatabase.buildDatabase(context)
    }

    @Singleton
    @Provides
    @Named(INTERNATIONALIZATION_DAO)
    fun provideInternationalizationDao(
        @Named(TIN_PARTNER_DATABASE)
        database: TruckerDatabase
    ) = database.internationalizationDao()
}