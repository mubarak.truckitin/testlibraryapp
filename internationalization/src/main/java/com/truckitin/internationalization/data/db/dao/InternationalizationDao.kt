package com.truckitin.internationalization.data.db.dao

import androidx.room.*
import com.truckitin.internationalization.data.db.entities.InternationalizationEntity

/**
 * Defines database operations.
 */
@Dao
interface InternationalizationDao {
    @Query("SELECT * FROM internationalization_table")
    suspend fun getListOfLocale(): List<InternationalizationEntity>

    @Query("SELECT * FROM internationalization_table WHERE `key`=(:key)")
    suspend fun getLocaleByKey(key: String): InternationalizationEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertInternationalizationList(list: List<InternationalizationEntity>)

    @Query("DELETE FROM internationalization_table")
    suspend fun deleteAllInternationalization()
}