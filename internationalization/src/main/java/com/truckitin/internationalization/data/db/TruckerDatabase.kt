package com.truckitin.internationalization.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.truckitin.internationalization.data.db.dao.InternationalizationDao
import com.truckitin.internationalization.data.db.entities.InternationalizationEntity
import com.truckitin.internationalization.utils.Constants

/**
 * Database for storing all local data.
 */

@Database(entities = [InternationalizationEntity::class], version = 2)
abstract class TruckerDatabase : RoomDatabase() {
    abstract fun internationalizationDao(): InternationalizationDao

    class AppMigrations : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE internationalization_table ADD COLUMN eng TEXT DEFAULT NULL")
        }
    }

    companion object {

        fun buildDatabase(context: Context): TruckerDatabase = Room.databaseBuilder(
            context,
            TruckerDatabase::class.java,
            Constants.TIN_PARTNER_DATABASE
        )
            .fallbackToDestructiveMigration()
            .addMigrations(AppMigrations())
            .build()
    }
}