package com.truckitin.internationalization.data

import com.truckitin.internationalization.data.db.dao.InternationalizationDao
import com.truckitin.internationalization.data.db.entities.InternationalizationEntity
import com.truckitin.internationalization.utils.Constants.INTERNATIONALIZATION_DAO
import javax.inject.Inject
import javax.inject.Named

class InternationalizationRepository @Inject constructor(
    @Named(INTERNATIONALIZATION_DAO) private val internationalizationDao: InternationalizationDao,
) {

    /**
     * Insert OR Update in database.
     */
    suspend fun upsertInternationalizationList(list: List<InternationalizationEntity>) {
        internationalizationDao.upsertInternationalizationList(list)
    }

    /**
     * Returns recorded list from database.
     */
    suspend fun getListOfLocale() =
        internationalizationDao.getListOfLocale()

    /**
     * Returns recorded list from database.
     */
    suspend fun getLocaleByKey(key: String) =
        internationalizationDao.getLocaleByKey(key)

    /**
     * Delete all from database.
     */
    suspend fun deleteAllInternationalization() {
        internationalizationDao.deleteAllInternationalization()
    }

}