package com.truckitin.internationalization.data.db.entities

import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey


@Keep
@Entity(tableName = "internationalization_table")
data class InternationalizationEntity(
    @PrimaryKey
    val key: String,
    val ur: String,
    val eng: String?
)
