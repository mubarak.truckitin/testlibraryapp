package com.truckitin.internationalization.utils

import java.util.concurrent.TimeUnit

object Constants {
    const val INTERVAL_INTERNATIONALIZATION = 24L
    val TIMEUNIT_INTERNATIONALIZATION = TimeUnit.HOURS
    const val INTERNATIONALIZATION_WORK_NAME = "com.truckitin.internationalization.worker.SendLocationsToServerWorker"
    const val TIN_PARTNER_DATABASE = "tin_partner_Database"
    const val INTERNATIONALIZATION_DAO = "internationalization_dao"
}