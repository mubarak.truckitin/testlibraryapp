package com.truckitin.preferences

import com.truckitin.base.LoggerUtil

sealed class Language(val code: String) {
    companion object {
        fun get(languageCode: String): Language = when (languageCode) {
            "en" -> English
            "ur" -> Urdu
            "am" -> Amharic
            else -> {
                LoggerUtil.debug("[Truck] IllegalArgumentException, Not expecting language $languageCode")
                English
            }
        }
    }
}

object English : Language("en")
object Urdu : Language("ur")
object Amharic : Language("am")
