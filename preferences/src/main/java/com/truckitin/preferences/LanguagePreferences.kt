package com.truckitin.preferences

import android.content.*
import com.truckitin.preferences.PreferenceKeys.USER_LANGUAGE
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.*

private const val APP_PREF_NAME = "TRUCK_IT_IN"

/**
 * this class is added as courotines are not working for our language change behaviour,
 * as we need to run a blocking call in on attach base context before onCreate
 */
@Singleton
class LanguagePreferences @Inject constructor(@ApplicationContext context: Context) {

    private val prefs: SharedPreferences by lazy {
        context.getSharedPreferences(APP_PREF_NAME, 0)
    }
    var appLang: String
        get() = prefs[USER_LANGUAGE, English.code]
        set(value) {
            prefs[USER_LANGUAGE] = value
        }
}
