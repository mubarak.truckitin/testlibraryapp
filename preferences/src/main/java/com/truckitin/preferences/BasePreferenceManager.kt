package com.truckitin.preferences

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.clear
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.preferencesKey
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Base class used for storage of user preferences,
 * this implementation uses proto by jetpack
 */
abstract class BasePreferenceManager {
    @Inject
    lateinit var gson: Gson

    abstract suspend fun clear(block: ((Boolean) -> Unit)? = null)

    /**
     * Method to clear all Datastore preferences(Key-Values pairs) provided
     *
     * @param dataStore         array of dataStores
     * @param block             callback function - Optional
     */
    suspend fun clearAll(
        dataStore: Array<out DataStore<Preferences>>,
        block: ((Boolean) -> Unit)? = null
    ) {
        withContext(Dispatchers.IO) {
            try {
                for (data in dataStore) {
                    data.edit { it.clear() }
                }
                block?.let {
                    withContext(Dispatchers.Main) { it(true) }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                block?.let {
                    withContext(Dispatchers.Main) { it(false) }
                }
            }
        }
    }

    /**
     * Generic method to save key-value pairs in preferences
     *
     * @param key         Key to store in preference
     * @param value       Generic Typed value to store against key
     * @param dataStore   DataStore preference in which the key-value to be stored
     * @param block       Callback to receive the success/fail storage operation
     */
    suspend inline fun <reified T : Any> save(
        key: String,
        value: T,
        dataStore: DataStore<Preferences>,
        noinline block: ((Boolean) -> Unit)? = null
    ) {
        withContext(Dispatchers.IO) {
            try {
                val dataStoreKey = preferencesKey<T>(key)
                dataStore.edit { preference ->
                    preference[dataStoreKey] = value
                    block?.let {
                        withContext(Dispatchers.Main) { it(true) }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                block?.let {
                    withContext(Dispatchers.Main) { it(false) }
                }
            }
        }
    }

    /**
     * Generic method to get values from DataStore preference
     *
     * @param key             Key provide to retrieve value
     * @param datastore       DataStore preference from which the value to be retrieve
     * @param defaultValue    Default value will be return in-case of any error
     *
     * @return T?          Generic typed value or null if not found
     */
    suspend inline fun <reified T : Any> getValue(
        key: String,
        datastore: DataStore<Preferences>,
        defaultValue: T? = null
    ): T? {
        return withContext(Dispatchers.IO) {
            try {
                val dataStoreKey = preferencesKey<T>(key)
                val preferences = datastore.data.first()
                withContext(Dispatchers.Main) {
                    preferences[dataStoreKey]
                }
            } catch (e: Exception) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    defaultValue
                }
            }
        }
    }

    /**
     * Generic method to get values from DataStore preference in Flow typed
     *
     * @param key             Key provide to retrieve value
     * @param datastore       DataStore preference from which the value to be retrieve
     * @param defaultValue    Default value will be return in-case of any error
     *
     * @return Flow<T>     Generic typed value as flow
     */
    inline fun <reified T : Any> getValueAsFlow(
        key: String,
        dataStore: DataStore<Preferences>,
        defaultValue: T? = null
    ): Flow<T?> =
        dataStore.data
            .catch { exception ->
                exception.printStackTrace()
                emit(emptyPreferences())
            }
            .map { preference ->
                val dataStoreKey = preferencesKey<T>(key)
                preference[dataStoreKey] ?: defaultValue
            }

    /**
     * Generic method to get values from DataStore preference in Flow typed
     *
     * @param key             Key provide to retrieve value
     * @param dataStore       DataStore preference from which the value to be retrieve
     * @param defaultValue    Default value will be return in-case of any error
     *
     * @return Flow<T>     Generic typed value as flow
     */
    inline fun <reified T : Any?> getObjectAsFlow(
        key: String,
        dataStore: DataStore<Preferences>,
        defaultValue: T? = null
    ): Flow<T?> =
        dataStore.data
            .catch { exception ->
                exception.printStackTrace()
                emit(emptyPreferences())
            }
            .map { preference ->
                val dataStoreKey = preferencesKey<String>(key)
                val json = preference[dataStoreKey]
                if (!json.isNullOrEmpty()) {
                    gson.fromJson(json, object : TypeToken<T>() {}.type) as T?
                } else {
                    defaultValue
                }
            }
}
