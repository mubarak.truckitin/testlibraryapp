package com.truckitin.preferences

object PreferenceKeys {
    const val USER_LANGUAGE: String = "USER_LANGUAGE"
}
