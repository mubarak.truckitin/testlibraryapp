package com.truckitin.core.composeutils

import androidx.annotation.*
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.*
import androidx.compose.foundation.text.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.*
import androidx.compose.ui.graphics.*
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.*
import androidx.compose.ui.text.*
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.*
import androidx.compose.ui.unit.*
import androidx.compose.ui.window.DialogProperties
import com.google.accompanist.swiperefresh.*
import com.truckitin.core.R

@Composable
fun ComposeText(
    modifier: Modifier = Modifier,
    @StringRes text: Int,
    style: TextStyle = TinDefaultTextStyle(),
    maxLines: Int = Int.MAX_VALUE,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    onTextLayout: (TextLayoutResult) -> Unit = {},
) {
    ComposeText(
        modifier = modifier,
        text = stringResource(id = text),
        style = style,
        maxLines = maxLines,
        overflow = overflow,
        softWrap = softWrap,
        onTextLayout = onTextLayout,
    )
}

@Composable
fun ComposeText(
    modifier: Modifier = Modifier,
    text: String,
    style: TextStyle = TinDefaultTextStyle(),
    maxLines: Int = Int.MAX_VALUE,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    onTextLayout: (TextLayoutResult) -> Unit = {},
) {
    BasicText(
        modifier = modifier,
        text = text,
        style = style,
        maxLines = maxLines,
        overflow = overflow,
        softWrap = softWrap,
        onTextLayout = onTextLayout,
    )
}

@Composable
fun ComposeAnnotatedText(
    modifier: Modifier = Modifier,
    text: AnnotatedString,
    style: TextStyle = TinDefaultTextStyle(),
    maxLines: Int = Int.MAX_VALUE,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    onTextLayout: (TextLayoutResult) -> Unit = {},
) {
    BasicText(
        modifier = modifier,
        text = text,
        style = style,
        maxLines = maxLines,
        overflow = overflow,
        softWrap = softWrap,
        onTextLayout = onTextLayout,
    )
}

@Composable
fun ComposeClickableText(
    modifier: Modifier = Modifier,
    @StringRes text: Int,
    style: TextStyle = TinDefaultTextStyle(),
    maxLines: Int = Int.MAX_VALUE,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    onTextLayout: (TextLayoutResult) -> Unit = {},
    onClick: (Int) -> Unit = {},
) {
    ComposeClickableText(
        modifier = modifier,
        text = stringResource(text),
        style = style,
        maxLines = maxLines,
        overflow = overflow,
        softWrap = softWrap,
        onTextLayout = onTextLayout,
        onClick = onClick
    )
}

@Composable
fun ComposeClickableText(
    modifier: Modifier = Modifier,
    text: String,
    style: TextStyle = TinDefaultTextStyle(),
    maxLines: Int = Int.MAX_VALUE,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    onTextLayout: (TextLayoutResult) -> Unit = {},
    onClick: (Int) -> Unit = {}
) {
    ComposeClickableAnnotatedText(
        modifier = modifier,
        text = AnnotatedString(text),
        style = style,
        maxLines = maxLines,
        overflow = overflow,
        softWrap = softWrap,
        onTextLayout = onTextLayout,
        onClick = onClick
    )
}

@Composable
fun ComposeClickableAnnotatedText(
    modifier: Modifier = Modifier,
    text: AnnotatedString,
    style: TextStyle = TinDefaultTextStyle(),
    maxLines: Int = Int.MAX_VALUE,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    onTextLayout: (TextLayoutResult) -> Unit = {},
    onClick: (Int) -> Unit = {}
) {
    ClickableText(
        modifier = modifier,
        text = text,
        style = style,
        maxLines = maxLines,
        overflow = overflow,
        softWrap = softWrap,
        onTextLayout = onTextLayout,
        onClick = onClick
    )
}

@Composable
fun ComposeIcon(
    modifier: Modifier = Modifier,
    @DrawableRes icon: Int,
    contentDescription: String? = null,
    tint: Color = Color.Unspecified
) {
    Icon(
        modifier = modifier,
        painter = painterResource(icon),
        contentDescription = contentDescription,
        tint = tint
    )
}

@Composable
fun ComposeButton(
    modifier: Modifier = Modifier,
    @StringRes text: Int,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        backgroundColor = colorResource(R.color.colorPrimary),
        contentColor = colorResource(R.color.white)
    ),
    shape: Shape = MaterialTheme.shapes.small,
    contentPadding: PaddingValues = ButtonDefaults.ContentPadding,
    enabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    elevation: ButtonElevation? = ButtonDefaults.elevation(),
    border: BorderStroke? = null,
    onClick: () -> Unit = {}
) {
    ComposeButton(
        modifier = modifier,
        onClick = onClick,
        colors = colors,
        shape = shape,
        contentPadding = contentPadding,
        enabled = enabled,
        interactionSource = interactionSource,
        elevation = elevation,
        border = border,
        text = stringResource(text)
    )
}

@Composable
fun ComposeButton(
    modifier: Modifier = Modifier,
    text: String,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        backgroundColor = colorResource(R.color.colorPrimary),
        contentColor = colorResource(R.color.white)
    ),
    shape: Shape = MaterialTheme.shapes.small,
    contentPadding: PaddingValues = ButtonDefaults.ContentPadding,
    enabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    elevation: ButtonElevation? = ButtonDefaults.elevation(),
    border: BorderStroke? = null,
    onClick: () -> Unit = {}
) {
    Button(
        modifier = modifier,
        onClick = onClick,
        colors = colors,
        shape = shape,
        contentPadding = contentPadding,
        enabled = enabled,
        interactionSource = interactionSource,
        elevation = elevation,
        border = border,
    ) {
        Text(text)
    }
}

@Composable
fun ComposeImage(
    @DrawableRes image: Int,
    modifier: Modifier = Modifier,
    alignment: Alignment = Alignment.Center,
    contentScale: ContentScale = ContentScale.Fit,
    alpha: Float = DefaultAlpha,
    colorFilter: ColorFilter? = null
){
    Image(
        modifier = modifier,
        painter = painterResource(image),
        alignment = alignment,
        contentScale = contentScale,
        alpha = alpha,
        colorFilter = colorFilter,
        contentDescription = ""
    )
}

@Composable
fun ComposeSwipeRefresh(
    swipeRefreshState: SwipeRefreshState,
    onRefresh: () -> Unit,
    modifier: Modifier = Modifier,
    swipeEnabled: Boolean = true,
    refreshTriggerDistance: Dp = 80.dp,
    indicatorAlignment: Alignment = Alignment.TopCenter,
    indicatorPadding: PaddingValues = PaddingValues(0.dp),
    clipIndicatorToPadding: Boolean = true,
    content: @Composable () -> Unit,
){
    SwipeRefresh(
        state = swipeRefreshState,
        onRefresh = onRefresh,
        modifier = modifier,
        swipeEnabled = swipeEnabled,
        refreshTriggerDistance = refreshTriggerDistance,
        indicatorAlignment = indicatorAlignment,
        indicatorPadding = indicatorPadding,
        indicator = { state, trigger ->
            SwipeRefreshIndicator(
                state = state,
                refreshTriggerDistance = trigger,
                scale = true,
                backgroundColor = colorResource(id = R.color.bg_gray),
                shape = CircleShape,
            )
        },
        clipIndicatorToPadding = clipIndicatorToPadding,
        content = content,
    )
}

@Composable
fun EmptyView(
    modifier: Modifier = Modifier,
    @DrawableRes icon: Int = R.drawable.ic_no_booking,
    @StringRes text: Int,
    isButtonVisible: Boolean = false,
    buttonText: String = "",
    buttonClick: () -> Unit = {}
) {
    EmptyView(
        modifier = modifier,
        icon = {ComposeImage(icon)},
        text = {
            ComposeText(
                text = stringResource(text),
                style = TinDefaultTextStyle(
                    fontSize = R.dimen.normal_text,
                    textAlign = TextAlign.Center,
                )
            )
        },
        button = if (isButtonVisible){
            {
                ComposeButton(
                    modifier = Modifier
                        .padding(top = dimensionResource(R.dimen._16sdp)),
                    text = buttonText,
                    shape = RoundedCornerShape(percent = 50),
                    onClick = buttonClick
                )
            }
        }else null
    )
}

@Composable
fun ComposeOutlineTextField(
    modifier: Modifier = Modifier,
    columnModifier: Modifier = Modifier,
    errorModifier: Modifier = Modifier
        .padding(start = 16.dp)
        .fillMaxWidth(),
    textState: MutableState<String> = mutableStateOf(""),
    onValueChange: (String) -> Unit,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = TinDefaultTextStyle(),
    @StringRes label: Int? = null,
    @StringRes placeholder: Int? = null,
    @DrawableRes leadingIcon: Int? = null,
    @ColorRes leadingIconTint: Int? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    singleLine: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    shape: Shape = MaterialTheme.shapes.small,
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors(
        focusedBorderColor = colorResource(R.color.colorPrimary),
        focusedLabelColor = colorResource(R.color.colorPrimary),
        cursorColor = colorResource(R.color.colorPrimary),
        errorCursorColor = colorResource(R.color.colorPrimary),
        errorBorderColor = colorResource(R.color.red),
        errorLabelColor = colorResource(R.color.red),
    ),
    @StringRes
    errorText: Int? = null
) {
    Column(modifier = columnModifier) {
        OutlinedTextField(
            value = textState.value,
            onValueChange = {
                textState.value = it
                onValueChange.invoke(it)
            },
            modifier = modifier,
            enabled = enabled,
            readOnly = readOnly,
            textStyle = textStyle,
            label = label?.let {
                { Text(stringResource(it)) }
            },
            placeholder = placeholder?.let {
                { Text(stringResource(it)) }
            },
            leadingIcon = leadingIcon?.let {
                {
                    Icon(
                        painter = painterResource(it),
                        contentDescription = "",
                        tint = leadingIconTint?.let {
                            colorResource(id = it)
                        } ?: LocalContentColor.current.copy(alpha = LocalContentAlpha.current)
                    )
                }
            },
            trailingIcon = trailingIcon,
            isError = isError,
            visualTransformation = visualTransformation,
            keyboardOptions = keyboardOptions,
            keyboardActions = keyboardActions,
            singleLine = singleLine,
            maxLines = maxLines,
            interactionSource = interactionSource,
            shape = shape,
            colors = colors,
        )
        if (isError) {
            Text(
                text = stringResource(errorText ?: 0),
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = errorModifier
            )
        }
    }
}

@Composable
fun ComposeOutlineTextField(
    modifier: Modifier = Modifier,
    columnModifier: Modifier = Modifier,
    errorModifier: Modifier = Modifier,
    textState: MutableState<String> = mutableStateOf(""),
    onValueChange: (String) -> Unit,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = TinDefaultTextStyle(),
    @StringRes label: Int? = null,
    isError: Boolean = false,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    singleLine: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    shape: Shape = MaterialTheme.shapes.small,
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors(
        focusedBorderColor = colorResource(R.color.colorPrimary),
        focusedLabelColor = colorResource(R.color.colorPrimary),
        cursorColor = colorResource(R.color.colorPrimary),
        errorCursorColor = colorResource(R.color.colorPrimary),
        errorBorderColor = colorResource(R.color.red),
        errorLabelColor = colorResource(R.color.red),
    ),
    @StringRes
    errorText: Int? = null
) {
    Column(modifier = columnModifier) {
        OutlinedTextField(
            value = textState.value,
            onValueChange = {
                textState.value = it
                onValueChange.invoke(it)
            },
            modifier = modifier,
            enabled = enabled,
            readOnly = readOnly,
            textStyle = textStyle,
            label = label?.let {
                { Text(stringResource(it)) }
            },
            isError = isError,
            visualTransformation = visualTransformation,
            keyboardOptions = keyboardOptions,
            keyboardActions = keyboardActions,
            singleLine = singleLine,
            maxLines = maxLines,
            interactionSource = interactionSource,
            shape = shape,
            colors = colors,
        )
        if (isError) {
            Text(
                text = stringResource(errorText ?: 0),
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = errorModifier
            )
        }
    }
}

@Composable
fun EmptyView(
    modifier: Modifier = Modifier,
    icon: @Composable () -> Unit = {
        ComposeImage(R.drawable.ic_no_booking)
    },
    text: @Composable () -> Unit = {
        ComposeText(
            text = stringResource(R.string.no_booking),
            style = TinDefaultTextStyle(
                fontSize = R.dimen.normal_text,
                textAlign = TextAlign.Center,
            )
        )
    },
    button: @Composable (() -> Unit)? = null
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(
                start = dimensionResource(id = R.dimen._16sdp),
                end = dimensionResource(id = R.dimen._16sdp),
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        icon()
        text()
        button?.invoke()
    }
}

@Composable
fun ComposeAlert(
    state: MutableState<Boolean> = mutableStateOf(false),
    onDismissRequest: () -> Unit = { state.value = false },
    confirmButtonText: String = stringResource(R.string.yes),
    confirmButtonColor: Int = R.color.colorPrimary,
    @DimenRes confirmButtonSize: Int = R.dimen.large_text,
    onConfirmClick: () -> Unit = {},
    modifier: Modifier = Modifier,
    dismissButtonText: String = stringResource(R.string.no),
    dismissButtonColor: Int = R.color.text_light,
    @DimenRes dismissButtonSize: Int = R.dimen.large_text,
    onDismissClick: () -> Unit = {},
    title: String = stringResource(R.string.alert),
    @DimenRes titleFontSize: Int = R.dimen.large_text,
    @ColorRes titleTextColor: Int = R.color.colorPrimary,
    content: String? = null,
    @DimenRes contentFontSize: Int = R.dimen.small,
    @ColorRes contentTextColor: Int = R.color.black,
    shape: Shape = MaterialTheme.shapes.medium,
    backgroundColor: Color = MaterialTheme.colors.surface,
    contentColor: Color = contentColorFor(backgroundColor),
    properties: DialogProperties = DialogProperties(),
){
    ComposeAlert(
        state = state,
        onDismissRequest = onDismissRequest,
        confirmButton = {
            TextButton(
                onClick = {
                    onConfirmClick()
                    state.value = false
                }
            ){
                Text(
                    modifier = Modifier
                        .padding(end = dimensionResource(id = R.dimen._5sdp)),
                    text = confirmButtonText,
                    fontSize = dimensionResource(confirmButtonSize).value.sp,
                    color = colorResource(confirmButtonColor)
                )
            }
        },
        modifier = modifier.fillMaxWidth(),
        dismissButton = {
            TextButton(
                onClick = {
                    onDismissClick()
                    state.value = false
                }
            ){
                Text(
                    modifier = Modifier
                        .padding(start = dimensionResource(id = R.dimen._8sdp)),
                    text = dismissButtonText,
                    fontSize = dimensionResource(dismissButtonSize).value.sp,
                    color = colorResource(dismissButtonColor)
                )
            }
        },
        title = {
            ComposeText(
                modifier = Modifier
                    .fillMaxWidth(),
                text = title,
                style = TinDefaultTextStyle(
                    fontSize = titleFontSize,
                    color = titleTextColor,
                    textAlign = TextAlign.Start,
                )
            )
        },
        text = content?.let {
            {
                ComposeText(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = dimensionResource(id = R.dimen._8sdp)),
                    text = it,
                    style = TinDefaultTextStyle(
                        fontSize = contentFontSize,
                        color = contentTextColor,
                        textAlign = TextAlign.Start
                    )
                )
            }
        },
        shape = shape,
        backgroundColor = backgroundColor,
        contentColor = contentColor,
        properties = properties
    )
}

@Composable
fun ComposeAlert(
    state: MutableState<Boolean> = mutableStateOf(false),
    onDismissRequest: () -> Unit = { state.value = false },
    confirmButton: @Composable () -> Unit = {
        ComposeText(
            modifier = Modifier.padding(horizontal = dimensionResource(id = R.dimen._8sdp)),
            text = stringResource(id = R.string.yes),
            style = TinDefaultTextStyle(
                fontSize = R.dimen.normal_text,
                color = R.color.colorPrimary
            )
        )
    },
    modifier: Modifier = Modifier,
    dismissButton: @Composable (() -> Unit)? = {
        ComposeText(
            modifier = Modifier.padding(horizontal = dimensionResource(id = R.dimen._8sdp)),
            text = stringResource(id = R.string.no),
            style = TinDefaultTextStyle(
                fontSize = R.dimen.normal_text
            )
        )
    },
    title: @Composable (() -> Unit)? = {
        ComposeText(
            text = stringResource(id = R.string.alert),
            style = TinDefaultTextStyle(
                fontSize = R.dimen.normal_text
            )
        )
    },
    text: @Composable (() -> Unit)? = null,
    shape: Shape = MaterialTheme.shapes.medium,
    backgroundColor: Color = MaterialTheme.colors.surface,
    contentColor: Color = contentColorFor(backgroundColor),
    properties: DialogProperties = DialogProperties(),
) {
    if (state.value){
        AlertDialog(
            onDismissRequest = onDismissRequest,
            buttons = {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(all = 8.dp),
                    horizontalArrangement = Arrangement.End,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    dismissButton?.invoke()
                    confirmButton()
                }
            },
            modifier = modifier.fillMaxWidth(),
            title = title,
            text = text,
            shape = shape,
            backgroundColor = backgroundColor,
            contentColor = contentColor,
            properties = properties,
        )
    }
}
