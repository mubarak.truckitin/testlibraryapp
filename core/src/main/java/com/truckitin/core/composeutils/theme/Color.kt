package com.truckitin.core.composeutils.theme

import androidx.compose.ui.graphics.Color

val colorPrimary = Color(0XFF4088CC)
val colorPrimarySecond = Color(0xFFE95758)
val colorPrimaryThird = Color(0xFFFFAA55)
val colorPrimaryGradient = Color(0xFF53A9EE)
val currency = Color(0x61000000)
val bg_gray = Color(0xFFF2F2F2)
val bg_gray2 = Color(0xFFF5F7F9)
val gray_box = Color(0xFFABABAB)
val text_color_dark = Color(0xDE000000)
val text_light = Color(0xFF606060)
val red = Color(0xFFE53536)
val black = Color.Black
val white = Color.White