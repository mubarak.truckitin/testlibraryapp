package com.truckitin.core.composeutils.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.MaterialTheme.shapes
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.intl.Locale
import com.truckitin.core.manager.LocaleManager
import com.truckitin.preferences.Language
import com.truckitin.preferences.Urdu

private val LightColorPalette = lightColors(
    primary = colorPrimary,
    secondary = colorPrimary,
    surface = white,
    background = bg_gray,
    primaryVariant = colorPrimarySecond,
    secondaryVariant = colorPrimarySecond,
    onPrimary = colorPrimaryThird,
    onSecondary = colorPrimaryThird,
    onSurface = black,
    onBackground = black,
)

private val DarkColorPalette = darkColors(
    primary = colorPrimary,
    secondary = colorPrimary,
    surface = bg_gray,
    primaryVariant = colorPrimarySecond,
    secondaryVariant = colorPrimarySecond,
    onPrimary = colorPrimaryThird,
    onSecondary = colorPrimaryThird,
    onSurface = colorPrimaryGradient
)

@Composable
fun TINTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    isLocaleUrdu: Boolean = true,
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = LightColorPalette,
        typography = if (isLocaleUrdu) typographyUrdu else typography,
        shapes = shapes,
        content = content
    )
}