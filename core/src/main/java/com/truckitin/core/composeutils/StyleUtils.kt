package com.truckitin.core.composeutils

import androidx.annotation.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.*
import androidx.compose.ui.text.intl.LocaleList
import androidx.compose.ui.text.style.*
import androidx.compose.ui.unit.*
import com.truckitin.core.R
import com.truckitin.core.composeutils.theme.englishFont
import com.truckitin.core.composeutils.theme.urduFont
import com.truckitin.preferences.LanguagePreferences
import com.truckitin.preferences.Urdu

@Composable
fun isUrduLocale(): Boolean {
    val context = LocalContext.current
    val languagePreferences = LanguagePreferences(context)
    return languagePreferences.appLang == Urdu.code
}

@Composable
fun TinDefaultTextStyle(
    @ColorRes color: Int = R.color.text_light,
    @DimenRes fontSize: Int = R.dimen.small,
    textAlign: TextAlign? = null,
    fontStyle: FontStyle? = null,
    fontWeight: FontWeight? = null,
    fontFamily: FontFamily? = null,
    letterSpacing: TextUnit = TextUnit.Unspecified,
    textDecoration: TextDecoration? = null,
    lineHeight: TextUnit = TextUnit.Unspecified,
    fontSynthesis: FontSynthesis? = null,
    fontFeatureSettings: String? = null,
    baselineShift: BaselineShift? = null,
    textGeometricTransform: TextGeometricTransform? = null,
    localeList: LocaleList? = null,
    background: Color = Color.Unspecified,
    shadow: Shadow? = null,
    textDirection: TextDirection? = null,
    textIndent: TextIndent? = null
): TextStyle {
    return TextStyle(
        fontSize = dimensionResource(fontSize).value.sp,
        color = colorResource(color),
        textAlign = textAlign,
        fontWeight = fontWeight,
        fontStyle = fontStyle,
        fontFamily = fontFamily ?: if (isUrduLocale()) urduFont else englishFont,
        letterSpacing = letterSpacing,
        textDecoration = textDecoration,
        lineHeight = lineHeight,
        fontSynthesis = fontSynthesis,
        fontFeatureSettings = fontFeatureSettings,
        baselineShift = baselineShift,
        textGeometricTransform = textGeometricTransform,
        localeList = localeList,
        background = background,
        shadow = shadow,
        textDirection = textDirection,
        textIndent = textIndent,
    )
}