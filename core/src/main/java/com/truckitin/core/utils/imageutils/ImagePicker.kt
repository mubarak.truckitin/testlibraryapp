package com.truckitin.core.utils.imageutils

import android.os.Environment
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts.*
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import dagger.hilt.android.scopes.FragmentScoped
import java.io.*


/**
 * This ImagePicker class depended on fragment
 * Usage:
 *
 * register with lifecycle:
 *
 *  private val imagePicker by lazy {
 *      ImagePicker(requireActivity())
 *  }
 *
 *  add observer in oncreate
 *  lifecycle.addObserver(imagePicker)
 *
 *
 **/
const val PICKER_KEY = "Tin_Image_Picker"
const val CAPTURE_KEY = "Tin_Image_Capture"

@FragmentScoped
class ImagePicker(
    @FragmentScoped private val activity: FragmentActivity
) : DefaultLifecycleObserver{

    private val _imageResultURI = MutableLiveData<File>()
    val imageResultURI: LiveData<File> = _imageResultURI

    private lateinit var pickImage : ActivityResultLauncher<String>

    override fun onCreate(owner: LifecycleOwner) {
        pickImage = activity.activityResultRegistry.register(PICKER_KEY, owner, GetContent()) { uri ->
            uri?.let {
                _imageResultURI.postValue(FileUriUtils.getFileFromUri(activity, it))
            }
        }
    }

    fun selectImage() = pickImage.launch("image/*")

    fun takeImage() {
        val photoFile: File? = createImageFile()
        // Continue only if the File was successfully created
        photoFile?.also {
            val photoURI = FileProvider.getUriForFile(
                activity,
                "${activity.applicationContext.packageName}.provider",
                it
            )
            activity.activityResultRegistry.register(CAPTURE_KEY, TakePicture()) { isSuccess ->
                if (isSuccess) {
                    _imageResultURI.postValue(it)
                }
            }.launch(photoURI)
        }
    }

    private fun createImageFile() =
        try {
            File.createTempFile(
                "JPEG_",
                ".jpg",
                activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            )
        }catch (ex: IOException) {
            null
        }
}