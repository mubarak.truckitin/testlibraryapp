package com.truckitin.core.utils

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.databinding.ViewDataBinding
import com.truckitin.core.R

fun getAlertDialog(
    context: Context,
    db: ViewDataBinding
): AlertDialog {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    builder.setView(db.root)
    val dialog = builder.create()
    dialog.setCanceledOnTouchOutside(false)
    return dialog
}

fun getAlertDialog(
    context: Context,
    layout: Int
): AlertDialog {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    val customLayout: View = LayoutInflater.from(context).inflate(layout, null)
    builder.setView(customLayout)
    val dialog = builder.create()
    dialog.setCanceledOnTouchOutside(false)
    return dialog
}

@JvmOverloads
fun showAlert(
    context: Context,
    @StringRes title: Int,
    @StringRes message: Int,
    @DrawableRes icon: Int? = null,
    @StringRes positiveBtnMsg: Int,
    positiveBtnAction: () -> Unit = {},
    @StringRes negativeBtnMsg: Int? = null,
    negativeBtnAction: () -> Unit = {}
) {
    val negativeMsg: String? = negativeBtnMsg?.let {
        context.getString(it)
    }

    showAlert(
        context,
        context.getString(title),
        context.getString(message),
        icon,
        context.getString(positiveBtnMsg),
        positiveBtnAction = positiveBtnAction,
        negativeMsg,
        negativeBtnAction
    )
}

@JvmOverloads
fun showAlert(
    context: Context,
    textTitle: String,
    textMessage: String,
    @DrawableRes icon: Int? = null,
    positiveBtnMsg: String,
    positiveBtnAction: () -> Unit = {},
    negativeBtnMsg: String? = null,
    negativeBtnAction: () -> Unit = {}
) {
    val builder = androidx.appcompat.app.AlertDialog.Builder(context, R.style.AlertDialogTheme)
        .setTitle(textTitle)
        .setCancelable(false)
        .setMessage(textMessage)
        .setPositiveButton(
            positiveBtnMsg
        ) { dialog, _ ->
            positiveBtnAction.invoke()
            dialog.dismiss()
        }
    icon?.let {
        builder.setIcon(icon)
    }
    negativeBtnMsg?.let {
        builder.setNegativeButton(negativeBtnMsg) { dialog, _ ->
            negativeBtnAction.invoke()
            dialog.dismiss()
        }
    }
    val dialog = builder.create()
    val textView = dialog.findViewById<TextView>(android.R.id.message)
    textView?.textSize = context.resources.getDimension(R.dimen.large_text)
    dialog.show()
}

fun showError(context: Context, @StringRes errorMessage: Int) {
    showError(context = context, errorMessage = context.getString(errorMessage))
}

@JvmOverloads
fun showError(context: Context, errorMessage: String) {
    showAlert(context = context, textTitle = context.getString(R.string.alert), textMessage = errorMessage, positiveBtnMsg = context.getString(R.string.ok))
}

fun showActionError(context: Context, errorMessage: String,positiveBtnAction: () -> Unit = {}) {
    showAlert(context = context, textTitle = context.getString(R.string.alert), textMessage = errorMessage, positiveBtnMsg = context.getString(R.string.ok), positiveBtnAction = positiveBtnAction)
}

fun showToast(view: View, @StringRes message: Int) {
    showToast(view.context, view.getString(message))
}

fun showToast(context: Context, @StringRes message: Int) {
    showToast(context, context.getString(message))
}

fun showToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}
