package com.truckitin.core.utils

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import com.truckitin.core.SingleLiveEvent
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject
import javax.inject.Singleton

@ActivityScoped
open class ToolbarEventsObserver @Inject constructor() {

    private val screenTitle = ObservableField("")
    private val screenDescription = ObservableField("")
    private var shouldShowBack = ObservableBoolean(false)
    private var showCampaignBtn = ObservableBoolean(false)
    private var showEdit = ObservableBoolean(false)
    private var titleIcon = ObservableBoolean(false)


    private val actionCTA = ObservableField("")
    private var showActionCTA = ObservableBoolean(false)

    private val _onEditClick = SingleLiveEvent<Boolean>()
    val onEditClick: LiveData<Boolean> = _onEditClick

    fun onEditClick(clicked: Boolean) {
        _onEditClick.value = clicked
    }

    fun setBackButtonVisibility(showBackButton: Boolean) {
        shouldShowBack.set(showBackButton)
    }

    open fun enableCampaignButton() {
        showCampaignBtn.set(true)
    }

    open fun disableCampaignButton() {
        showCampaignBtn.set(false)
    }

    open fun enableDisableEditButton(enabled: Boolean) {
        showEdit.set(enabled)
    }

    open fun enableActionCTAButton(enabled: Boolean) {
        showActionCTA.set(enabled)
    }

    open fun enableTitleIcon(enabled: Boolean) {
        titleIcon.set(enabled)
    }

    fun backButtonObservable() = shouldShowBack

    fun campaignButtonObservable() = showCampaignBtn

    fun editButtonObservable() = showEdit

    fun showActionCTAObservable() = showActionCTA

    fun titleIconObservable() = titleIcon

    open fun setScreenTitle(title: String) {
        screenTitle.set(title)
    }

    fun getScreenTitle() = screenTitle

    open fun setScreenDescription(description: String) {
        screenDescription.set(description)
    }

    fun getScreenDescription() = screenDescription

    open fun setActionCTA(buttonText: String) {
        actionCTA.set(buttonText)
    }

    fun getActionCTA() = actionCTA

}
