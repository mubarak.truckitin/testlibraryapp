package com.truckitin.core.utils

import android.app.ActivityManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import com.truckitin.core.appconstants.AppConstants.GMAPs_URI
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


fun openMaps(context: Context, latitude: Double, longitude: Double) {
    val uri = "$GMAPs_URI$latitude,$longitude"
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
    intent.setPackage("com.google.android.apps.maps")
    context.startActivity(intent)
}


fun openMaps(context: Context) {
    val mapIntent = Intent(Intent.ACTION_VIEW)
    mapIntent.setPackage("com.google.android.apps.maps")
    context.startActivity(mapIntent)
}

fun openAppSettings(context: Context) {
    val appSettingIntent = Intent().apply {
        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        data = Uri.fromParts("package", context.packageName, null)
    }
    context.startActivity(appSettingIntent)
}

fun openAppInPlayStore(context: Context, packageName: String) {
    try {
        context.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("market://details?id=$packageName")
            )
        )
    } catch (e: ActivityNotFoundException) {
        context.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
            )
        )
    }
}

fun Context.triggerRebirth() {
    startActivity(
        Intent.makeRestartActivityTask(
            packageManager.getLaunchIntentForPackage(
                packageName
            )?.component
        )
    )
    Runtime.getRuntime().exit(0)
}


fun isAppIsInBackground(context: Context): Boolean {
    var isInBackground = true
    val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    val runningProcesses = am.runningAppProcesses
    for (processInfo in runningProcesses) {
        if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
            for (activeProcess in processInfo.pkgList) {
                if (activeProcess == context.packageName) {
                    isInBackground = false
                }
            }
        }
    }
    return isInBackground
}


fun runWithDelay(time: Long, action: () -> Unit) {
    CoroutineScope(Dispatchers.Main).launch {
        delay(time)
        action.invoke()
    }
}