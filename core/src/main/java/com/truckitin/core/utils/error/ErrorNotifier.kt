package com.truckitin.core.utils.error

import android.content.Context
import androidx.annotation.StringRes
import com.truckitin.base.ErrorResponse
import com.truckitin.base.Resource
import com.truckitin.core.*
import com.truckitin.core.utils.showActionError
import com.truckitin.core.utils.showError
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ErrorNotifier @Inject constructor(private val errorParser: ErrorParser){

    fun handleError(context: Context, resource: Resource.ResourceError) {
        showError(context = context, context.getString(fetchErrorMessage(resource)))
    }

    fun handleActionError(context: Context, resource: Resource.ResourceError,positiveBtnAction: () -> Unit = {}) {
        showActionError(context = context, context.getString(fetchErrorMessage(resource)),positiveBtnAction=positiveBtnAction)
    }

    fun errorRes(throwable: Throwable) =
        fetchErrorMessage(errorParser.parseError(throwable))

    @StringRes
    fun fetchErrorMessage(resource: Resource.ResourceError) : Int{
       return when (resource) {
            is Resource.ResourceError.GenericError -> {
                resource.error?.let {
                    extractLocalisedErrorMessage(it)
                } ?: run {
                    R.string.generic_error
                }
            }
            else -> {
                R.string.no_network
            }
        }
    }

    @StringRes
    private fun extractLocalisedErrorMessage(error: ErrorResponse): Int {
        return when (error.code) {
            AuthErrorType.DEFAULT_BAD_CREDENTIALS_ERROR.code -> R.string.auth_001
            AuthErrorType.PASSWORD_MISMATCH_ERROR.code -> R.string.auth_002
            AuthErrorType.INVALID_USER_GRANTS_ERROR.code -> R.string.auth_003
            AuthErrorType.INVALID_AUTHENTICATION_TOKEN_ERROR.code -> R.string.auth_004
            AuthErrorType.USER_ALREADY_EXISTS.code -> R.string.auth_005
            AuthErrorType.DRIVER_ALREADY_EXISTS.code -> R.string.auth_006

            NotFoundErrorType.NO_VEHICLE_FOUND.code -> R.string.VH_001
            NotFoundErrorType.VEHICLE_TYPE_NOT_FOUND_ERROR.code -> R.string.VH_002
            NotFoundErrorType.VEHICLE_TYPE_VARIANT_NOT_FOUND_ERROR.code -> R.string.VH_003
            NotFoundErrorType.DRIVER_UNAVAILABLE.code -> R.string.BOOK_010
            NotFoundErrorType.VEHICLE_UNAVAILABLE.code -> R.string.BOOK_011
            NotFoundErrorType.DRIVER_NOT_FOUND_ERROR.code -> R.string.DR_001
            NotFoundErrorType.USER_NOT_FOUND_ERROR.code -> R.string.USR_001
            NotFoundErrorType.COUNTRY_NOT_FOUND_ERROR.code -> R.string.CN_001
            NotFoundErrorType.CITY_NOT_FOUND_ERROR.code -> R.string.CT_001
            NotFoundErrorType.COMPANY_NOT_FOUND_ERROR.code -> R.string.CO_001
            NotFoundErrorType.PRICING_COMPONENT_NOT_FOUND_ERROR.code -> R.string.PC_001
            NotFoundErrorType.FLEET_NOT_FOUND_ERROR.code -> R.string.FLT_001
            NotFoundErrorType.GOODS_TYPE_NOT_FOUND_ERROR.code -> R.string.GT_001
            NotFoundErrorType.ROLE_NOT_FOUND_ERROR.code -> R.string.ROL_001
            NotFoundErrorType.BASE_RATE_NOT_FOUND_ERROR.code -> R.string.BR_001
            NotFoundErrorType.REGISTRATION_NUMBER_ALREADY_EXIST.code -> R.string.VH_005
            NotFoundErrorType.NO_FILE_SELECTED.code -> R.string.FIL_001
            NotFoundErrorType.INVALID_OTP.code -> R.string.OTP_002
            NotFoundErrorType.CAMPAIGN_NOT_FOUND_ERROR.code -> R.string.CAMP_001

            BidErrorType.BID_NOT_FOUND_ERROR.code -> R.string.BID_001
            BidErrorType.BID_ALREADY_CREATED_FOR_BOOKING_ERROR.code -> R.string.BID_002
            BidErrorType.SHIPPER_CANNOT_BID_ERROR.code -> R.string.BID_003
            BidErrorType.BID_AMOUNT_MUST_BE_GREATER_THAN_BASE_RATE.code -> R.string.BID_004
            BidErrorType.BID_TIME_WINDOW_FOR_THIS_BOOKING_CLOSED.code -> R.string.BID_005
            BidErrorType.BID_AMOUNT_CAN_NOT_BE_UPDATED_AFTER_ACCEPTANCE.code -> R.string.BID_006
            BidErrorType.BID_WITH_INVALID_UNIQUE_ID_ERROR.code -> R.string.BID_012
            BidErrorType.BID_INVALID_STATE_OF_ACCEPTANCE_ERROR.code -> R.string.BID_013
            BidErrorType.BID_SHIPPER_REQUIREMENT_FULFILLED_ERROR.code -> R.string.BID_014
            BidErrorType.BID_REJECTION_ON_SHIPMENT_FAILURE.code -> R.string.BID_017
            BidErrorType.BID_INVALID_STATE_OF_DRIVER_ASSIGNMENT_ERROR.code -> R.string.BID_018
            BidErrorType.BIDDING_CLOSED_ON_SHIPMENT.code -> R.string.BID_020

            MISC.COLLECTION_AMOUNT.code ->  R.string.BOOK_009
            MISC.NO_VEHICLE_FOUND.code ->  R.string.VH_001
            MISC.NO_BOOKING_FOUND.code ->  R.string.BOOK_001
            MISC.BOOKING_NOT_STARTED.code ->  R.string.BOOK_019
            MISC.NO_BOOKING_ASSOCIATED.code ->  R.string.BOOK_020
            MISC.UNABLE_TO_DELETE_VEHICLE.code ->  R.string.VH_008
            MISC.BANK_ACCOUNT_INVALID_IBAN.code ->  R.string.GE_004
            MISC.BANK_ACCOUNT_EXISTING_IBAN.code ->  R.string.BANK_002
            MISC.BOOKING_STATUS_INCONSISTENCY.code ->  R.string.BOOK_006

            LedgerErrorType.USER_LEDGER_ERROR.code ->  R.string.LEDG_001
            LedgerErrorType.UNAUTHORIZED_REQUEST_ERROR_LEDGER.code ->  R.string.LEDG_002
            LedgerErrorType.ONBOARDONG_ERROR_LEDGER.code ->  R.string.LEDG_002
            LedgerErrorType.ENTITY_TRANSACTION_REQUEST_ERROR.code ->  R.string.ETR001
            LedgerErrorType.TRANSACTION_WITH_OWN_ACCOUNT.code ->  R.string.ETR001
            else -> R.string.generic_error
        }
    }
}
