package com.truckitin.core.utils

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat

fun TextView.setDrawAbleStart(@DrawableRes id: Int) {
    setDrawableStart(getDrawable(id))
}

fun TextView.setDrawableStart(drawable: Drawable?) {
    TextViewCompat.setCompoundDrawablesRelativeWithIntrinsicBounds(
        this,
        drawable,
        null,
        null,
        null
    )
}

fun TextView.setDrawAbleStartWithTint(@DrawableRes id: Int, @ColorRes colorId: Int) {
    getDrawable(id)?.let {
        it.setTint(getColor(colorId))
        setDrawableStart(it)
    }
}

fun TextView.changeTextColor(@ColorRes id: Int) {
    setTextColor(getColor(id))
}

fun TextView.setTextViewDrawableColor(color: Int) {
    for (drawable in this.compoundDrawables) {
        drawable?.let {
            it.colorFilter =
                PorterDuffColorFilter(
                    ContextCompat.getColor(this.context, color),
                    PorterDuff.Mode.SRC_IN
                )
        }
    }
}
