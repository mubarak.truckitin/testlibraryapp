package com.truckitin.core.utils

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText

/**
 * @param lambda after text changed is invoked after removing textwatcher
 * so that one can set data in the view
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    val editText = this
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            editText.removeTextChangedListener(this)
            afterTextChanged.invoke(editable.toString())
            editText.addTextChangedListener(this)
        }
    })
}

fun EditText.onFocusChange(onFocusChange: (Boolean) -> Unit) {
    this.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus -> onFocusChange.invoke(hasFocus) }
}
