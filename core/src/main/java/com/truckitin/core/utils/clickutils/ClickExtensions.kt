package com.truckitin.core.utils.clickutils

import android.os.SystemClock
import android.view.View
import androidx.compose.runtime.*

fun View.clicks(onSafeClick: (View) -> Unit) {
    val safeClickListener = SingleShotListener {
        onSafeClick(it)
    }
    setOnClickListener(safeClickListener)
}

fun safeClick(
    defaultInterval: Int = 1000,
    state: MutableState<Long> = mutableStateOf(0),
    onSafeClick: () -> Unit
){
    if (SystemClock.elapsedRealtime() - state.value < defaultInterval){
        return
    }
    state.value = SystemClock.elapsedRealtime()
    onSafeClick()
}