package com.truckitin.core.utils.error

import com.google.gson.Gson
import com.truckitin.base.ErrorResponse
import com.truckitin.base.Resource
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class ErrorParser @Inject constructor(private val gson: Gson) {

    fun parseError(throwable: Throwable): Resource.ResourceError {
        return when (throwable) {
            is IOException -> Resource.ResourceError.NetworkError(throwable = throwable)
            is HttpException -> {
                val errorResponse = convertErrorBody(throwable)
                if (throwable.code() == AUTH_EXPIRY) {
                    return Resource.ResourceError.AuthExpiry(throwable = throwable)
                }

                return Resource.ResourceError.GenericError(errorResponse, throwable)
            }
            else -> {
                Resource.ResourceError.GenericError(ErrorResponse(message = throwable.message), throwable)
            }
        }
    }

    private fun convertErrorBody(throwable: HttpException): ErrorResponse? {
        val json: String? = throwable.response()?.errorBody()?.string()
        return try {
            gson.fromJson(json, ErrorResponse::class.java)
        } catch (exception: Exception) {
            null
        }
    }

    companion object {
        private const val AUTH_EXPIRY = 429 //for now error codes are not in place for backend, hence we have agreed upon 429 to act as token expiry
    }
}