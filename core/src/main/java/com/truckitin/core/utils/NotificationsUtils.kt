package com.truckitin.core.utils

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.core.app.NotificationCompat

fun buildNotification(
    context: Context,
    notificationId: Int,
    notification: Notification
){
    val notificationManager = getNotificationManager(context)
    notificationManager.notify(notificationId, notification)
}

fun getNotification(
    context: Context,
    channelId: String,
    channelName: String,
    importanceLvl: Int,
    channelDesc: String,
    notificationBuilder: NotificationCompat.Builder
): Notification {
    val notificationManager = getNotificationManager(context)

    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
        val level = when (importanceLvl) {
            1 -> NotificationManager.IMPORTANCE_MIN
            2 -> NotificationManager.IMPORTANCE_LOW
            3 -> NotificationManager.IMPORTANCE_DEFAULT
            4 -> NotificationManager.IMPORTANCE_HIGH
            5 -> NotificationManager.IMPORTANCE_MAX
            else -> NotificationManager.IMPORTANCE_UNSPECIFIED
        }

        notificationManager.createNotificationChannel(
            createNotificationChannel(channelId, channelName, level, channelDesc)
        )
    }

    return notificationBuilder.build()
}

private fun getNotificationManager(context: Context): NotificationManager =
    context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

@TargetApi(android.os.Build.VERSION_CODES.O)
private fun createNotificationChannel(
    channelID: String,
    channelName: String,
    importanceLvl: Int,
    description: String,
    enableLights: Boolean = false,
    @ColorInt lightColor: Int = Color.CYAN,
    enableVibration: Boolean = false
): NotificationChannel {

    return NotificationChannel(channelID, channelName, importanceLvl)
        .apply {
            enableLights(enableLights)
            setLightColor(lightColor)
            enableVibration(enableVibration)
            setDescription(description)
        }
}

fun getNotificationPendingIntent(context: Context): PendingIntent? {
    val notifyIntent = Intent().apply {
        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        setClassName(context, "com.truckitin.trucker.splash.SplashActivity")
    }
    return PendingIntent.getActivity(
        context,
        0,
        notifyIntent,
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    )
}