package com.truckitin.core.utils

import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

fun View.getString(@StringRes id: Int): String = context.getString(id)

fun View.getString(@StringRes id: Int, resIds: String): String = context.getString(id).format(resIds)

fun View.getString(@StringRes id: Int, resId: String, resId2: String): String = context.getString(id).format(resId, resId2)

fun View.getColor(@ColorRes id: Int): Int = context.getColor(id)

fun View.getDrawable(@DrawableRes id: Int): Drawable? = ContextCompat.getDrawable(context, id)

fun View.setTint(@ColorRes color: Int) {
    backgroundTintList = ColorStateList.valueOf(getColor(color))
}
