package com.truckitin.core.utils

import java.text.DecimalFormat


fun currencyFormatter(num: Double): String {
    val formatter = DecimalFormat("###,###,###")
    return formatter.format(num)
}

fun String.getAmount() : String{
    return this.replace(",", "")
}