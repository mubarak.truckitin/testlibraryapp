package com.truckitin.core.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager


const val HELP_LINE = "+923211771070"
const val TERMS_AND_CONDITIONS = "https://truckitin.com/privacy-policy/"
fun Context.openCallerToCall(number: String) {
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = Uri.parse("tel:$number")
    startActivity(intent)
}

fun Context.callHelpLine() {
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = Uri.parse("tel:$HELP_LINE")
    startActivity(intent)
}

fun Context.hideKeyboardFrom(view: View) {
    val imm: InputMethodManager =
        getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

/**
 * Open a web page of a specified URL
 * @param url URL to open
 */

fun Context.openWebPage(url: String) {
    val intent = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_BROWSER)
    intent.data = Uri.parse(url)
    if (intent.resolveActivity(packageManager) != null) {
        startActivity(intent)
    }
    else {
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {}

    }
}

fun Context.openTermsAndConditions(){
    openWebPage(TERMS_AND_CONDITIONS)
}
