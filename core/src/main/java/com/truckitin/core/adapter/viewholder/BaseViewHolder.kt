package com.truckitin.core.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Base class to be used for every class that wants to implement viewholder for recylerview
 * @param <T> the type of the Data model that has been used for UI binding
 */
abstract class BaseViewHolder<T>(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun onBind(position: Int, data: T, listener: (data: T) -> Unit)
}
