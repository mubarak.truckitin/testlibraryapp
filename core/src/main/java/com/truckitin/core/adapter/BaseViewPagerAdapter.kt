package com.truckitin.core.adapter

import android.content.Context
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.truckitin.core.base.BaseFragment

abstract class BaseViewPagerAdapter(
    fragment: Fragment,
    private val mFragments: Array<Fragment>
): FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int {
        return mFragments.size // Number of fragments displayed
    }

    fun getName(context: Context, position: Int) = context.getString(
        (mFragments[position] as (BaseFragment<*>)).screenName()
    )

    @NonNull
    override fun createFragment(position: Int): Fragment {
        return mFragments[position]
    }
}