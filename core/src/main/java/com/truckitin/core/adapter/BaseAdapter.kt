package com.truckitin.core.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.truckitin.core.adapter.diffutil.DiffUtilCallback
import com.truckitin.core.adapter.viewholder.BaseViewHolder
import com.truckitin.core.comparator.BaseItemComparator
import com.truckitin.core.comparator.ItemComparator
import com.truckitin.core.disposables.DisposeDisposables
import com.truckitin.core.extensions.addDisposable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Base class to be used for every class that wants to implement adapters for recylerview
 * @param <T> the type of the Data model that has been used for UI binding
 */
abstract class BaseAdapter<T>(
    protected val dataItems: ArrayList<T> = ArrayList(),
    private val comparator: ItemComparator<T> = BaseItemComparator(),
    private val listener: (item: T) -> Unit = {}
) :
    RecyclerView.Adapter<BaseViewHolder<T>>() {

    private val disposables: DisposeDisposables = DisposeDisposables()

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T>

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.onBind(position, dataItems[position], listener)
    }

    override fun getItemCount(): Int {
        return dataItems.size
    }

    fun onItemRemoved(position: Int) {
        dataItems.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itemCount)
    }

    fun update(items: List<T>) {
        if (dataItems.isEmpty()) {
            updateAllItems(items)
        } else {
            updateDiffItemsOnly(items)
        }
    }

    fun updateItem(item: T) {
        updateDiffItemsOnly(listOf(item))
    }
    // todo remove rx java and add channels, coroutine
    fun updateAllItems(items: List<T>) {
        Single.just(items)
            .doOnSuccess { data: List<T> ->
                updateItemsInModel(data)
            }
            .doOnSuccess {
                notifyDataSetChanged()
            }
            .subscribe({}, {}).addDisposable(disposables)
    }

    private fun updateDiffItemsOnly(items: List<T>) {
        Single.fromCallable { calculateDiff(items) }
            .subscribeOn(Schedulers.io())
            .doOnSuccess { updateItemsInModel(items) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::updateAdapterWithDiffResult) {}.addDisposable(disposables)
    }

    private fun calculateDiff(newItems: List<T>): DiffUtil.DiffResult {
        return DiffUtil.calculateDiff(DiffUtilCallback(dataItems, newItems, comparator))
    }

    private fun updateItemsInModel(items: List<T>) {
        dataItems.clear()
        dataItems.addAll(items)
    }

    private fun updateAdapterWithDiffResult(result: DiffUtil.DiffResult) {
        result.dispatchUpdatesTo(this)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        disposables.dispose()
    }
}
