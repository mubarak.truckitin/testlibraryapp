package com.truckitin.core.comparator

/**
 * Base class used by all comparators implementing diff util mechanism
 * @param <T> the type of the Data model to be compared
 */
class BaseItemComparator<T> : ItemComparator<T> {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: T, newItem: T): Any? {
        return null
    }
}
