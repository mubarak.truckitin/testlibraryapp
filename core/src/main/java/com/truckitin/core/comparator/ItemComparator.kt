package com.truckitin.core.comparator

/**
 * Base class used by all comparators, it has the methods which computes wether two objects are same or not
 * @param <T> the type of the Data model to be compared
 */
interface ItemComparator<T> {
    fun areItemsTheSame(oldItem: T, newItem: T): Boolean
    fun areContentsTheSame(oldItem: T, newItem: T): Boolean
    fun getChangePayload(oldItem: T, newItem: T): Any?
}
