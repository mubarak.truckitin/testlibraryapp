package com.truckitin.core.disposables

import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * If we are to use rx java, this class comes in handy to dispose
 * calls if ui is getting cleared
 */
class DisposeDisposables @Inject constructor() : Dispose {

    private val disposables: MutableList<Disposable?> = mutableListOf()

    fun add(disposable: Disposable?) {
        disposables.add(disposable)
    }

    override fun dispose() {
        disposables.forEach {
            if (it?.isDisposed == false) {
                it.dispose()
            }
        }
        disposables.clear()
    }
}
