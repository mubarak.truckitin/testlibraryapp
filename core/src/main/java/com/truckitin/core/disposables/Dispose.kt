package com.truckitin.core.disposables

interface Dispose {
    fun dispose()
}
