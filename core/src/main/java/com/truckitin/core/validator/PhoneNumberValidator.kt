package com.truckitin.core.validator

import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber

object PhoneNumberValidator {

    private val phoneUtil = PhoneNumberUtil.getInstance()

    fun isValidNumber(region: String, number: String): Boolean {
        val regionNumber = numberParser(region, number)
        return regionNumber?.let {
            phoneUtil.isValidNumber(regionNumber)
        } ?: kotlin.run {
            false
        }
    }

    fun countryCodeFormatter(region: String, phoneNumber: String): String {
        val regionNumber = numberParser(region, phoneNumber.trimStart('+').replace(" ", ""))
        return regionNumber?.let {
            phoneUtil.format(regionNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
        } ?: kotlin.run {
            phoneNumber
        }
    }

    private fun numberParser(region: String, phoneNumber: String): Phonenumber.PhoneNumber? {
        return try {
            phoneUtil.parse(phoneNumber, region)
        } catch (ex: Exception) {
            com.truckitin.base.LoggerUtil.debug("[Truck] Number parsing exception${ex.message}")
            null
        }
    }
}
