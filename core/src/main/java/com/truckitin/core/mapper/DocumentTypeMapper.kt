package com.truckitin.core.mapper

import android.content.Context
import com.truckitin.common.enums.DocumentType
import com.truckitin.core.R

fun getNameOfDocument(context: Context, id: Int, default: String = ""): String {
    return when (id) {
        1 -> context.getString(R.string.builty)
        2 -> context.getString(R.string.proof_of_delivery)
        3 -> context.getString(R.string.container_release_order)
        4 -> context.getString(R.string.cnic)
        5 -> context.getString(R.string.driving_license)
        6 -> context.getString(R.string.vehicle_registration)
        7 -> context.getString(R.string.bill_of_lading)
        8 -> context.getString(R.string.delivery_order)
        9 -> context.getString(R.string.equipment_interchange_receipt)
        10 -> context.getString(R.string.weighbridge_slip)
        11 -> context.getString(R.string.gate_pass)
        12 -> context.getString(R.string.token)
        13 -> context.getString(R.string.advance_payment_details)
        14 -> context.getString(R.string.remainder_payment_details)
        15 -> context.getString(R.string.others)
        else -> default
    }
}

fun getDocumentTypeList() = DocumentType.BILTY.type.rangeTo(DocumentType.OTHERS.type).toList()
