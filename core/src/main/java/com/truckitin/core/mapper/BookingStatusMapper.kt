package com.truckitin.core.mapper

import android.content.Context
import com.truckitin.core.R
import com.truckitin.core.enums.BidStatus
import com.truckitin.core.enums.BookingStatus
import com.truckitin.core.utils.getString


fun getLocalizedBookingStatus(context: Context, bookingStatusType: Int, default: String = ""): String {
    return when (bookingStatusType) {
        BookingStatus.CREATED.type -> {
            context.getString(R.string.created)
        }

        BookingStatus.BID_ASSIGNED.type -> {
            context.getString(R.string.bid_assigned)
        }

        BookingStatus.ACKNOWLEDGED.type -> {
            context.getString(R.string.acknowledged)
        }

        BookingStatus.LEFT_FOR_LOADING.type -> {
            context.getString(R.string.left_for_loading)
        }
        BookingStatus.ARRIVED_FOR_LOADING.type -> {
            context.getString(R.string.arived_at_pickup)
        }
        BookingStatus.LOADING_STARTED.type -> {
            context.getString(R.string.loading_started)
        }
        BookingStatus.LOADING_COMPLETED.type -> {
            context.getString(R.string.loading_completed)
        }
        BookingStatus.LEFT_FOR_DROPOFF.type -> {
            context.getString(R.string.left_for_dropoff)
        }
        BookingStatus.ARRIVED_AT_DROPOFF.type -> {
            context.getString(R.string.arrived_at_dropoff)
        }
        BookingStatus.UNLOADING_STARTED.type -> {
            context.getString(R.string.unloading_started)
        }
        BookingStatus.UNLOADING_COMPLETED.type -> {
            context.getString(R.string.unloading_completed)
        }
        BookingStatus.COMPLETED.type -> {
            context.getString(R.string.completed)
        }
        BookingStatus.CANCELLED.type -> {
            context.getString(R.string.cancelled)
        }
        else -> default
    }
}

fun getLocalizedBidStatus(context: Context, bidStatusType: Int, default: String = ""): String {
    return when (bidStatusType) {
        BidStatus.CREATED.type -> {
            context.getString(R.string.created)
        }

        BidStatus.ACCEPTED.type -> {
            context.getString(R.string.bid_assigned)
        }

        BidStatus.ACKNOWLEDGED.type -> {
            context.getString(R.string.acknowledged)
        }

        BidStatus.BACKOUT.type -> {
            context.getString(R.string.cancelled)
        }

        BidStatus.REJECTED.type -> {
            context.getString(R.string.cancelled)
        }

        BidStatus.FULFILLED.type -> {
            context.getString(R.string.completed)
        }

        BidStatus.DRIVER_ASSIGNED.type -> {
            context.getString(R.string.driver_assigned_status)
        }
        else -> default
    }
}