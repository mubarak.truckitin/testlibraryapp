package com.truckitin.core.mapper

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.truckitin.core.R

/**
 * @param context
 * @param key, id of the goods type
 */
fun getGoodsTypeIcon(context: Context, key: Int?): Drawable? {
    val drawable = getGoodsIcon(key)
    return ContextCompat.getDrawable(context, drawable)
}

fun getGoodsIcon(key: Int?): Int {
    val drawable = when (key) {
        1 -> R.drawable.agriculture
        2 -> R.drawable.rice
        3 -> R.drawable.wheat
        4 -> R.drawable.automobiles
        5 -> R.drawable.cement
        6 -> R.drawable.coal
        7 -> R.drawable.cotton
        8 -> R.drawable.edible_oils
        9, 26 -> R.drawable.electronics
        10 -> R.drawable.fertilizer
        11 -> R.drawable.fmcg
        12 -> R.drawable.foam
        13, 14 -> R.drawable.furniture
        15, 27, 31 -> R.drawable.steel
        16 -> R.drawable.chemicals
        17 -> R.drawable.machinery
        18, 19 -> R.drawable.minerals
        20, 21, 28 -> R.drawable.paper
        22 -> R.drawable.perishables
        23 -> R.drawable.oil
        24 -> R.drawable.pharma
        25 -> R.drawable.pipe
        29 -> R.drawable.plastic
        30 -> R.drawable.shoes
        32 -> R.drawable.textile
        33 -> R.drawable.tiles
        34 -> R.drawable.tyres
        else -> R.drawable.other_goods
    }
    return drawable
}

/**
 * @param context
 * @param key, id of the goods type
 * @param defaultName return this name if no localise name is available for id
 */
fun getGoodTypeName(context: Context, key: Int, defaultName: String?): String {

    val data: Int? = when (key) {
        1 -> R.string.agriculture
        2 -> R.string.rice
        3 -> R.string.wheat
        4 -> R.string.automobiles
        5 -> R.string.cement
        6 -> R.string.coal
        7 -> R.string.cotton
        8 -> R.string.edible_oils
        9 -> R.string.electronics
        10 -> R.string.fertilizer
        11 -> R.string.fmcg
        12 -> R.string.foam
        13 -> R.string.furniture
        14 -> R.string.household_furniture
        15 -> R.string.iron_steel
        16 -> R.string.chemicals
        17 -> R.string.machinery
        18 -> R.string.minerals
        19 -> R.string.minerals_and_salt
        20 -> R.string.paper
        21 -> R.string.paper_and_board
        22 -> R.string.perishables
        23 -> R.string.oil
        24 -> R.string.pharma
        25 -> R.string.plastic
        26 -> R.string.refrigerated
        27 -> R.string.scrap_iron
        28 -> R.string.scrap_paper
        29 -> R.string.paper_plastic
        30 -> R.string.shoes
        31 -> R.string.steel
        32 -> R.string.textile
        33 -> R.string.tiles
        34 -> R.string.tyres
        else -> null
    }

    data?.let {
        return context.getString(data)
    } ?: run {
        return defaultName ?: ""
    }
}
