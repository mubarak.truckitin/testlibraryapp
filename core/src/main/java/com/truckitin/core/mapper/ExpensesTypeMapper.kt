package com.truckitin.core.mapper

import android.content.Context
import com.truckitin.core.R

// TODO, find the usages of this class and remove if not in use
fun getNameOfExpense(context: Context, id: Int, default: String = ""): String {
    return when (id) {
        1 -> context.getString(R.string.advance)
        2 -> context.getString(R.string.remaining)
        3 -> context.getString(R.string.shipper_commission)
        4 -> context.getString(R.string.trucker_commission)
        5 -> context.getString(R.string.shipper_tax)
        6 -> context.getString(R.string.trucker_tax)
        7 -> context.getString(R.string.selling)
        8 -> context.getString(R.string.buying)
        9 -> context.getString(R.string.discount)
        10 -> context.getString(R.string.premium)
        11 -> context.getString(R.string.fuel)
        12 -> context.getString(R.string.toll)
        13 -> context.getString(R.string.fines)
        14 -> context.getString(R.string.tyres)
        15 -> context.getString(R.string.other_expenses)
        else -> default
    }
}