package com.truckitin.core.mapper

import android.content.Context
import com.truckitin.core.R
import com.truckitin.core.enums.LoadingWindow
import com.truckitin.core.enums.PaymentWindow



fun getLocalizedLoadingTime(context: Context, loadingWindowType: Int): String{
    return when(loadingWindowType){
        LoadingWindow.IMMEDIATELY.type -> context.getString(R.string.immidiately)
        LoadingWindow.NIGHTLY.type, LoadingWindow.NIGHTLY_2.type -> context.getString(R.string.nightly)
        LoadingWindow.NIGHT_DAY.type, LoadingWindow.NIGHT_DAY_2.type -> context.getString(R.string.raat_din)
        else -> context.getString(R.string.din_raat)
    }
}

fun getLocalizedPaymentTime(context: Context, paymentWindowType: Int): String{
    return when(paymentWindowType){
        PaymentWindow.ADVANCE.type -> context.getString(R.string.advance)
        PaymentWindow.TO_PAY.type -> context.getString(R.string.to_pay)
        else -> context.getString(R.string.Paid)
    }
}