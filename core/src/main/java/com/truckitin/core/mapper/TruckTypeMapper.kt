package com.truckitin.core.mapper

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.truckitin.core.R

/**
 * if we dont have localised name we will return the default name un localised
 * @param context
 * @param id, id of the truck
 * @param default incase we dont have localised we wil return default
 */
fun getNameOfTruck(context: Context, id: Int, default: String): String {
    return when (id) {
        1 -> context.getString(R.string.containerized)
        2 -> context.getString(R.string.flatbed)
        3 -> context.getString(R.string.halfbody)
        4 -> context.getString(R.string.mazda)
        5 -> context.getString(R.string.suzuki)
        6 -> context.getString(R.string.shehzore)
        else -> default
    }
}

/**
 * if we dont have icon we will return pickup i,e, acts as generic one
 * @param context
 * @param id, id of the truck
 */
fun getIconOfTruck(context: Context, id: Int?): Drawable? {
    return ContextCompat.getDrawable(context, getTruckIcon(id))
}

fun getTruckIcon(
    id: Int?
): Int {
    return R.drawable.ic_truck
//    return when (id) {
//        1 -> R.drawable.container_01
//        2 -> R.drawable.flatbed
//        3 -> R.drawable.halfbody
//        4 -> R.drawable.mazda
//        5 -> R.drawable.pickup
//        6 -> R.drawable.shahzore
//        else -> R.drawable.pickup
//    }
}
