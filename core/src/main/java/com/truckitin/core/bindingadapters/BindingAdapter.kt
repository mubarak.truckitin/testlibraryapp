package com.truckitin.core.bindingadapters

import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.TranslateAnimation
import androidx.databinding.BindingAdapter
import com.truckitin.base.NAE

@BindingAdapter("slideWhileFadeUp")
fun slideWhileFadeUp(view: View, value: Boolean) {

    if (value) {
        if (view.visibility == View.VISIBLE) return
        val set = AnimationSet(true)
        val animate = TranslateAnimation(0f, 0f, view.height.toFloat() / 3, 0f)
        val alphaAnimation = AlphaAnimation(0f, 1f)
        alphaAnimation.duration = 250
        animate.duration = 250
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {}

            override fun onAnimationStart(animation: Animation?) {
                view.visibility = View.VISIBLE
            }
        })
        set.addAnimation(alphaAnimation)
        set.addAnimation(animate)

        view.startAnimation(set)
    } else {

        val set = AnimationSet(true)
        val animate = TranslateAnimation(0f, 0f, 0f, view.height.toFloat() / 3)
        val alphaAnimation = AlphaAnimation(1f, 0f)
        alphaAnimation.duration = 100
        animate.duration = 100
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {
            }

            override fun onAnimationStart(animation: Animation?) {
                view.visibility = View.GONE
            }
        })
        set.addAnimation(animate)
        set.addAnimation(alphaAnimation)

        view.startAnimation(set)
    }
}

fun fade(view: View, value: Boolean) {
    if (value) {
        if (view.visibility == View.VISIBLE) return
        val animation1 = AlphaAnimation(0f, 1.0f)
        animation1.duration = 350
        animation1.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
                view.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation?) {
            }

            override fun onAnimationRepeat(animation: Animation?) {
            }
        })
        view.startAnimation(animation1)
    } else {
        if (view.visibility == View.GONE) return
        val animation1 = AlphaAnimation(1.0f, 0f)
        animation1.duration = 350
        animation1.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                view.visibility = View.GONE
            }

            override fun onAnimationRepeat(animation: Animation?) {
            }
        })
        view.startAnimation(animation1)
    }
}

@BindingAdapter("slideWhileFadeStart")
fun slideWhileFadeStart(view: View, value: Boolean) {

    if (value) {
        if (view.visibility == View.VISIBLE) return
        val set = AnimationSet(true)
        val animate = TranslateAnimation(view.width.toFloat() / 3, 0f, 0f, 0f)
        val alphaAnimation = AlphaAnimation(0f, 1f)
        alphaAnimation.duration = 250
        animate.duration = 250
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {}

            override fun onAnimationStart(animation: Animation?) {
                view.visibility = View.VISIBLE
            }
        })
        set.addAnimation(alphaAnimation)
        set.addAnimation(animate)

        view.startAnimation(set)
    } else {

        val set = AnimationSet(true)
        val animate = TranslateAnimation(0f, view.width.toFloat() / 3, 0f, 0f)
        val alphaAnimation = AlphaAnimation(1f, 0f)
        alphaAnimation.duration = 100
        animate.duration = 100
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {
            }

            override fun onAnimationStart(animation: Animation?) {
                view.visibility = View.GONE
            }
        })
        set.addAnimation(animate)
        set.addAnimation(alphaAnimation)

        view.startAnimation(set)
    }
}

@BindingAdapter("slideWhileFadeEnd")
fun slideWhileFadeEnd(view: View, value: Boolean) {

    if (value) {
        if (view.visibility == View.VISIBLE) return
        val set = AnimationSet(true)
        val animate = TranslateAnimation((view.width.toFloat()) * -1, 0f, 0f, 0f)
        val alphaAnimation = AlphaAnimation(0f, 1f)
        alphaAnimation.duration = 250
        animate.duration = 250
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {}

            override fun onAnimationStart(animation: Animation?) {
                view.visibility = View.VISIBLE
            }
        })
        set.addAnimation(alphaAnimation)
        set.addAnimation(animate)

        view.startAnimation(set)
    } else {

        val set = AnimationSet(true)
        val animate = TranslateAnimation(0f, view.width.toFloat() * -1, 0f, 0f)

        val alphaAnimation = AlphaAnimation(1f, 0f)
        alphaAnimation.duration = 100
        animate.duration = 100
        animate.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {
                view.visibility = View.GONE
            }

            override fun onAnimationStart(animation: Animation?) {

            }
        })
        set.addAnimation(animate)
        set.addAnimation(alphaAnimation)

        view.startAnimation(set)
    }
}

fun View.slideAndFade(value: Boolean) {
    slideWhileFadeUp(this, value)
}

@BindingAdapter("shouldShow")
fun shouldShow(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}

@BindingAdapter("shouldShow")
fun shouldShow(view: View, value: String?) {
    value.NAE({
        view.visibility = View.VISIBLE
    }, {
        view.visibility = View.GONE
    })
}

/**
 * this method hides a view if condition is met, in else case it does nothing
 */
@BindingAdapter("hideView")
fun hideView(view: View, value: Boolean) {
    if (value) view.visibility = View.GONE
}
