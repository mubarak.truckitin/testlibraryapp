package com.truckitin.core.model

import androidx.annotation.Keep

@Keep
data class BookingPricingList(
    val id: Int?,
    val bookingId: Int?,
    val amount: Double?,
    val pricingComponentId: Int?,
    val pricingComponentName: String?,
    val creationDate: String?,
    val updateDate: String?,
    val isPaid: Boolean?,
    val remarks: String?,
    val bankAccountNumber: String?,
    val disbursementStatus: String?,
    val disbursementStatusId: Int?
)
