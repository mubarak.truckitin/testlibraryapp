package com.truckitin.core.model

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.truckitin.core.enums.BidStatus
import com.truckitin.core.enums.BookingStatus
import com.truckitin.core.enums.LoadingWindow
import com.truckitin.core.enums.PaymentWindow
import com.truckitin.core.model.AddaModel
import kotlinx.parcelize.Parcelize

@Keep
data class PostedBid(
    var bidAmount: Double,
    val bidId: Int,
    val bidStatus: BidStatus,
    val id: Int,
    val bookingId: Int,
    var bookingStatus: BookingStatus,
    var bookingPreviousStatus: BookingStatus,
    val consigneeName: String,
    val consigneeNumber: String,
    val creationDate: String,
    val dimensions: String,
    val driverId: Int?,
    val driverName: String?,
    val dropoffAddress: String,
    val dropoffCity: String,
    val dropoffLatitude: Double,
    val dropoffLongitude: Double,
    val dropoffType: String,
    val eta: String,
    val goodsType: String,
    val loadingTime: String,
    val loadingWindowType: LoadingWindow,
    val packaging: String,
    val paymentReceivingType: PaymentWindow,
    val pickupAddress: String,
    val pickupCity: String,
    val pickupLatitude: Double,
    val pickupLongitude: Double,
    val pickupType: String,
    val shipmentId: Int,
    val specialInstructions: String,
    val totalAmount: Double,
    val userId: Int,
    val vehicleType: String,
    val vehicleTypeVariant: String,
    val cityTravelRestrictionType: String,
    val goodsTypeId: Int,
    val goodsDescription: String?,
    val labourAvailabilityType: String,
    val pickupCityId: Int,
    val vehicleTypeId: Int,
    val vehicleTypeVariantId: Int,
    val dropoffCityId: Int,
    val weight: Double,
    val vehicleId: Int?,
    val vehicleRegistration: String?,
    val modelYear: String?,
    val modelName: String?,
    val driverNumber: String?,
    val bidStatusTimestamp: String,
    val competitiveBidAmount: Long = 0,
    val totalBidsPerBooking: Long = 0,
    val bidWindowStartDateTime: String?,
    val bidWindowEndDateTime: String?,
    val tagId: Int,
    var documentsAvailable: Boolean = false,
    var expensesAvailable: Boolean = false,
    var buyingAndSellingAvailable: Boolean = false,
    var verificationStatusTypeId: Int,
    var fraudReason: String,
    var fraudReasonTypeId: Int,
    var fraudMarkedDate: String,
    var fraudMarkedById: Int,
    var fraudMarkedByFullName: String,
    val bookingPricingList: List<BookingPricingList>?,
    var amountRequestedByPartner: Double? = null,
    var offeredWeight: Double? = null,
    var actualWeight: Double? = null,
    var amountNegotiatedWithPartner: Double? = null,
    val requestApprovalTypeId: Int? = null,
    val requestApprovalType: String? = null,
    val bidCategoryTypeId:Int?,
    val shipmentContainerDto:ShipmentContainerDto?,
    val co2Emission:Double? = 0.0,
    val travelledDistance:Double? = 0.0,
    val shipmentLocations: List<ShipmentLocationsResponseDto>
) {
    fun toAddaModel() = AddaModel(this)

    fun getVehicleName() = "$modelName | $vehicleRegistration"
}

@Keep
@Parcelize
data class ShipmentLocationsResponseDto(
    val id: Int,
    val pointOfContactName: String,
    val pointOfContactNumber: String,
    val cityId: Int,
    val cityName: String,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val locationType: String,
    val locationTypeName: String,
    val locationActionType: Int,
    val locationActionTypeName: String,
    val loads: List<LoadsDto>,
): Parcelable

@Keep
@Parcelize
data class LoadsDto(
    val id: Int,
    val weight: Double,
    val goodsTypeId: Int,
    val goodsType: String,
    val goodsTypeDescription: String
): Parcelable

@Keep
data class ShipmentContainerDto(
    var emptyReturnDate: String?,
    var detentionCharges: Double?=null,
    var returnLocation: String,
)