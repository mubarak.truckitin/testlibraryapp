package com.truckitin.core.di

import android.content.Context
import com.tilismtech.tellotalksdk.managers.TelloApiClient
import com.truckitin.core.R
import com.truckitin.core.manager.LocaleManager
import dagger.Module
import dagger.Provides
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    /**
     * since we need it in onattach, dagger runs after it we need this mechanism to get locale
     * helper in our on attach method in base activity
     */
    @EntryPoint
    @InstallIn(SingletonComponent::class)
    interface LocaleHelperEntry {
        val localeHelper: LocaleManager
    }

    @Singleton
    @Provides
    fun provideTelloApiClient(
        @ApplicationContext context: Context
    ) =
        TelloApiClient.Builder()
            .accessKey(context.getString(R.string.corporate_chat_access_key))
            .projectToken(context.getString(R.string.corporate_chat_project_token))
            .notificationIcon(R.drawable.splash_logo).build()
}
