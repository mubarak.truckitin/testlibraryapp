package com.truckitin.core.extensions

import com.truckitin.core.validator.PhoneNumberValidator

fun String.isValidPkNumber() : Boolean{
    return PhoneNumberValidator.isValidNumber("PK", this) && this.length == 11
}

fun String.isValidPassword() : Boolean{
    return this.length >= 6
}

fun String.formatPkNumber() : String{
    return PhoneNumberValidator
        .countryCodeFormatter("PK", this)
        .trimStart('+').replace(" ", "")
}