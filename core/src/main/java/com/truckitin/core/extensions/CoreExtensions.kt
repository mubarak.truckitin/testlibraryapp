package com.truckitin.core.extensions

import com.truckitin.core.disposables.DisposeDisposables
import io.reactivex.disposables.Disposable

fun Disposable.addDisposable(disposable: DisposeDisposables) {
    disposable.add(this)
}
