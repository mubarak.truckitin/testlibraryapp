package com.truckitin.core.extensions

import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.widget.EditText
import android.widget.TextView
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import com.google.android.material.textview.MaterialTextView

fun EditText.clearError() {
    error = null
}

fun TextInputLayout.clearError() {
    error = null
}

fun MaterialTextView.underline() {
    this.paintFlags = this.paintFlags or Paint.UNDERLINE_TEXT_FLAG
    this.paint.isAntiAlias = true
}
