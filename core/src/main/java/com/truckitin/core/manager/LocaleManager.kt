package com.truckitin.core.manager

import android.annotation.TargetApi
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.os.Build
import com.truckitin.preferences.Amharic
import com.truckitin.preferences.Language
import com.truckitin.preferences.LanguagePreferences
import com.truckitin.preferences.Urdu
import java.util.*
import javax.inject.Inject

class LocaleManager @Inject constructor(private val languagePreferences: LanguagePreferences) {

    fun isLocaleUrdu() = getLanguageCode() == Urdu.code
    fun isLocaleAmharic() = getLanguageCode() == Amharic.code
    fun getLanguageCode() = languagePreferences.appLang


    fun changeLanguage(lang: Language) {
        languagePreferences.appLang = lang.code
    }

    fun wrap(context: Context, language: String): Context {
        val config = context.resources.configuration
        val sysLocale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            getSystemLocale(config)
        } else {
            getSystemLocaleLegacy(config)
        }
        if (language != "" && sysLocale?.language != language) {
            val locale = Locale(language)
            Locale.setDefault(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setSystemLocale(config, locale)
            } else {
                setSystemLocaleLegacy(config, locale)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.setLayoutDirection(locale)
                context.createConfigurationContext(config)
            } else {
                context.resources.updateConfiguration(config, context.resources.displayMetrics)
            }

        }
        return ContextWrapper(context)
    }

    fun getSystemLocaleLegacy(config: Configuration): Locale? {
        return config.locale
    }

    @TargetApi(Build.VERSION_CODES.N)
    fun getSystemLocale(config: Configuration): Locale? {
        return config.locales[0]
    }

    private fun setSystemLocaleLegacy(config: Configuration, locale: Locale?) {
        config.locale = locale
    }

    @TargetApi(Build.VERSION_CODES.N)
    fun setSystemLocale(config: Configuration, locale: Locale?) {
        config.setLocale(locale)
    }

    fun wrapForService(context: Context, language: String) : Context{
        val config = context.resources.configuration
        val locale = Locale(language)
        Locale.setDefault(locale)
        config.setLocale(locale)
        return context.createConfigurationContext(config)
    }
}
