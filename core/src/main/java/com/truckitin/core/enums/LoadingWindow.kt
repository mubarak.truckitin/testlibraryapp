package com.truckitin.core.enums

import androidx.annotation.Keep

@Keep
enum class LoadingWindow(val type: Int) {
    IMMEDIATELY(1),
    NIGHTLY(2),
    NIGHT_DAY(3),
    NIGHT_DAY_2(4),
    DAY_NIGHT(5),
    NIGHTLY_2(6);
}
