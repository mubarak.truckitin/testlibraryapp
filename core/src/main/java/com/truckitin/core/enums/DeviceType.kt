package com.truckitin.core.enums

import androidx.annotation.Keep

@Keep
enum class DeviceType(val type: Int) {
    ANDROID(1),
    WEB(2),
    IOS(3);
}