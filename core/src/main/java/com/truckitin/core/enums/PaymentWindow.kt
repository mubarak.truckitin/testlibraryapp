package com.truckitin.core.enums

import androidx.annotation.Keep

@Keep
enum class PaymentWindow(val type: Int) {
    PAID(1), // PAID equals payment in CREDIT
    TO_PAY(2), // TO_PAY equals payment at DROPOFF
    ADVANCE(3);
}
