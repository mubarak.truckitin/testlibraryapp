package com.truckitin.core.enums

import androidx.annotation.Keep

@Keep
enum class AccountHolderType(val type: Int) {
    USER(1),
    DRIVER(2);
}