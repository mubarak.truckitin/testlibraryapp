package com.truckitin.common.enums

import androidx.annotation.Keep

@Keep
enum class DocumentType(val type: Int) {
    BILTY(1),
    PROOF_OF_DELIVERY(2),
    CONTAINER_RELEASE_ORDER(3),
    CNIC(4),
    DRIVING_LICENSE(5),
    VEHICLE_REGISTRATION(6),
    BILL_OF_LADING(7),
    DELIVERY_ORDER(8),
    EQUIPMENT_INTERCHANGE_RECEIPT(9),
    WEIGHBRIDGE_SLIP(10),
    GATE_PASS(11),
    TOKEN(12),
    ADVANCE_PAYMENT_DETAILS(13),
    REMAINDER_PAYMENT_DETAILS(14),
    OTHERS(15),
    TYRE_INVOICE(16),
    COMPANY_CARD(17)
}