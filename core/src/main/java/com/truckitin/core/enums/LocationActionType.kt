package com.truckitin.core.enums

import androidx.annotation.Keep

@Keep
enum class LocationActionType(val type: Int) {
    PICK_UP(1), DROP_OFF(2)
}