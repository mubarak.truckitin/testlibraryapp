package com.truckitin.core.enums

import androidx.annotation.Keep

@Keep
enum class BidStatus(val type: Int) {
    CREATED(1), // screen 1
    ACCEPTED(2), // screen 2
    ACKNOWLEDGED(3), // screen 3
    BACKOUT(4),
    REJECTED(5),
    FULFILLED(6),
    DRIVER_ASSIGNED(7);
}
