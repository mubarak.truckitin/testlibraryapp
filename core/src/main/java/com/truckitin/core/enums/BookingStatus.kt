package com.truckitin.core.enums

import androidx.annotation.Keep

@Keep
enum class BookingStatus(val type: Int) {
    CREATED(1),
    BID_ASSIGNED(2),
    ACKNOWLEDGED(3),
    LEFT_FOR_LOADING(4),
    ARRIVED_FOR_LOADING(5),
    LOADING_STARTED(6),
    LOADING_COMPLETED(7),
    LEFT_FOR_DROPOFF(8),
    ARRIVED_AT_DROPOFF(9),
    UNLOADING_STARTED(10),
    UNLOADING_COMPLETED(11),
    COMPLETED(12),
    CANCELLED(13);
}
