package com.truckitin.core.base

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.truckitin.core.R
import com.truckitin.core.di.AppModule
import com.truckitin.core.manager.LocaleManager
import com.truckitin.core.utils.getAlertDialog
import dagger.hilt.android.EntryPointAccessors
import javax.inject.Inject

/**
 * Base class used by Activities implementing the MVVM pattern.
 * @param <DB> the type of the Data Binding
 */
abstract class BaseActivity<DB : ViewDataBinding> : AppCompatActivity() {

    /**
     * @return [Int] get id for the layout resource
     */
    @LayoutRes
    abstract fun getLayoutRes(): Int

    fun getLayoutBinding(): DB = binding

    @Inject lateinit var localeHelper: LocaleManager

    protected val binding by lazy {
        DataBindingUtil.setContentView(this, getLayoutRes()) as DB
    }

    /**
     * If you want to do some initialization of resources, setting listeners
     * on your activity, you can override this.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        onInit(savedInstanceState)
    }

    @StringRes
    abstract fun screenName(): Int

    /**
     *  You need override this method.
     *  Initialize view model listen to live data of
     *  And you need to set viewModel to binding: binding.viewModel = viewModel
     *
     */
    abstract fun onInit(savedInstanceState: Bundle?)

    override fun attachBaseContext(newBase: Context) {
        val wrapper = EntryPointAccessors.fromApplication(
            newBase,
            AppModule.LocaleHelperEntry::class.java
        ).localeHelper
        val language: String = wrapper.getLanguageCode()
        super.attachBaseContext(wrapper.wrap(newBase, language))
    }

    private val progress: AlertDialog by lazy {
        getAlertDialog(this, R.layout.progress_dialog)
    }

    fun showProgress(msg: String?) {
        if (!isDestroyed && !isFinishing) {
            progress.setMessage(msg)
            progress.show()
        }
    }

    fun hideProgress() {
        progress.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        hideProgress()
    }
}
