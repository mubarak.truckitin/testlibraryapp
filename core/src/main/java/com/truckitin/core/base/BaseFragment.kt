package com.truckitin.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import com.truckitin.base.Loading
import com.truckitin.core.utils.ToolbarEventsObserver
import com.truckitin.core.utils.error.ErrorNotifier
import javax.inject.Inject

/**
 * Base class used by DialogFragments implementing the MVVM pattern.
 * @param <DB> the type of the Data Binding
 */
abstract class BaseFragment<DB : ViewDataBinding> :
    Fragment() {

    /**
     * @return [Int] get id for the layout resource
     */
    @LayoutRes
    abstract fun getLayoutRes(): Int
    @StringRes
    abstract fun screenName(): Int
    open lateinit var binding: DB

    @Inject
    lateinit var errorNotifier: ErrorNotifier

    @Inject
    lateinit var toolbarEventsObserver: ToolbarEventsObserver

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        super.onCreateView(inflater, container, savedInstanceState)
        binding.lifecycleOwner = this
        toolbarEventsObserver.setScreenTitle(requireContext().getString(screenName()))
        toolbarEventsObserver.setBackButtonVisibility(showBackButton())
        setLoader()
        initialize()

        return binding.root
    }

    private fun setLoader() {
        loader()?.observe(
            viewLifecycleOwner
        ) {
            when (it?.isInProgress) {
                true -> (activity as BaseActivity<*>).showProgress(it.message)
                false -> (activity as BaseActivity<*>).hideProgress()
            }
        }
    }

    abstract fun initialize()

    open fun loader(): LiveData<Loading>? {
        return null
    }

    open fun showBackButton() = false

    // TODO: this was added to override back button handling in fragment, since its not a good approach hence commenting it out untill a usecase where its
    // absolute necessary
    /*private fun setUpOnBackPress() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (!onBackPress()) {
                    this.isEnabled = false
                    requireActivity().onBackPressed()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this, // LifecycleOwner
            callback
        )
    }*/

   /* override fun onAttach(context: Context) {
        super.onAttach(context)
        setUpOnBackPress()
    }*/
}
