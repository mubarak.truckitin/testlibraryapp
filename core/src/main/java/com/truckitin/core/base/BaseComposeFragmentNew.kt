package com.truckitin.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import com.truckitin.base.Loading
import com.truckitin.core.manager.LocaleManager
import com.truckitin.core.utils.ToolbarEventsObserver
import com.truckitin.core.utils.error.ErrorNotifier
import com.truckitin.preferences.Urdu
import javax.inject.Inject

/**
 * Base class used by DialogFragments implementing the MVVM pattern.
 * @param <DB> the type of the Data Binding
 */
abstract class BaseComposeFragmentNew : Fragment() {

    @StringRes
    abstract fun screenName(): Int

    @Inject
    lateinit var localeHelper: LocaleManager

    @Inject
    lateinit var errorNotifier: ErrorNotifier

    @Inject
    lateinit var toolbarEventsObserver: ToolbarEventsObserver

    var view: ComposeView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        var updateObserver = false
        if (view == null) {
            updateObserver = true
        }
        view = makeView(localeHelper.getLanguageCode() == Urdu.code)
        if (updateObserver) {
            toolbarEventsObserver.setScreenTitle(requireContext().getString(screenName()))
            toolbarEventsObserver.setBackButtonVisibility(showBackButton())
            setLoader()
            initialize()
        }
        return view!!
    }

    private fun setLoader() {
        loader()?.observe(
            viewLifecycleOwner
        ) {
            when (it?.isInProgress) {
                true -> (activity as BaseActivity<*>).showProgress(it.message)
                false -> (activity as BaseActivity<*>).hideProgress()
            }
        }
    }

    open fun showBackButton() = false
    abstract fun initialize()
    abstract fun makeView(isLocaleUrdu: Boolean): ComposeView
    open fun loader(): LiveData<Loading>? {
        return null
    }

}
