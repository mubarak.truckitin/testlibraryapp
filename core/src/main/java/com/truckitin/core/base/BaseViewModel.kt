package com.truckitin.core.base

import androidx.lifecycle.ViewModel

/**
 * Base class used by all viewmodels so that we can have control
 *
 */
open class BaseViewModel : ViewModel()
