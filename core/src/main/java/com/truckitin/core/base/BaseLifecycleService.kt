package com.truckitin.core.base

import android.content.Intent
import androidx.lifecycle.LifecycleService

abstract class BaseLifecycleService: LifecycleService(){

    override fun onCreate() {
        super.onCreate()
        onInit()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startCommand(intent, flags, startId)
        return super.onStartCommand(intent, flags, startId)
    }

    abstract fun onInit()

    abstract fun startCommand(intent: Intent?, flags: Int, startId: Int)

}