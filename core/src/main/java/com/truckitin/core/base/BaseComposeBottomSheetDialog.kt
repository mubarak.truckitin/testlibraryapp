package com.truckitin.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.compose.ui.platform.ComposeView
import androidx.lifecycle.LiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.truckitin.base.Loading
import com.truckitin.core.R
import com.truckitin.core.manager.LocaleManager
import com.truckitin.core.utils.error.ErrorNotifier
import javax.inject.Inject

/**
 * Base class used by DialogFragments implementing the MVVM pattern.
 */
abstract class BaseComposeBottomSheetDialog : BottomSheetDialogFragment() {

    @Inject
    lateinit var localeHelper: LocaleManager

    @Inject
    lateinit var errorNotifier: ErrorNotifier

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TinBottomSheetDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = makeView(localeHelper.isLocaleUrdu())
        setLoader()
        initialize()
        return view
    }

    private fun setLoader() {
        loader()?.observe(
            viewLifecycleOwner
        ) {
            when (it?.isInProgress) {
                true -> (activity as BaseActivity<*>).showProgress(it.message)
                false -> (activity as BaseActivity<*>).hideProgress()
            }
        }
    }

    abstract fun initialize()
    abstract fun makeView(isLocaleUrdu: Boolean) : ComposeView
    open fun loader(): LiveData<Loading>? {
        return null
    }
}