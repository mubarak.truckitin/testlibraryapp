package com.truckitin.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.truckitin.core.utils.error.ErrorNotifier
import javax.inject.Inject

/**
 * Base class used by DialogFragments implementing the MVVM pattern.
 * @param <DB> the type of the Data Binding
 */
abstract class BaseDialogFragment<DB : ViewDataBinding> : DialogFragment() {
    /**
     * @return [Int] get id for the layout resource
     */
    @LayoutRes
    abstract fun getLayoutRes(): Int

    abstract fun initialize()

    @Inject
    lateinit var errorNotifier: ErrorNotifier

    protected val binding: DB by lazy {
        DataBindingUtil.inflate(layoutInflater, getLayoutRes(), null, false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.lifecycleOwner = this
        initialize()
        return binding.root
    }
}
