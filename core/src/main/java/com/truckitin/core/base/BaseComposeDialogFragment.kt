package com.truckitin.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.compose.ui.platform.ComposeView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.truckitin.base.Loading
import com.truckitin.core.manager.LocaleManager
import com.truckitin.core.utils.error.ErrorNotifier
import javax.inject.Inject

/**
 * Base class used by DialogFragments implementing the MVVM pattern.
 */
abstract class BaseComposeDialogFragment : DialogFragment() {

    @Inject
    lateinit var localeHelper: LocaleManager

    @Inject
    lateinit var errorNotifier: ErrorNotifier

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = makeView(localeHelper.isLocaleUrdu())
        initDialog()
        setLoader()
        initialize()
        return view
    }

    private fun setLoader() {
        loader()?.observe(
            viewLifecycleOwner
        ) {
            when (it?.isInProgress) {
                true -> (activity as BaseActivity<*>).showProgress(it.message)
                false -> (activity as BaseActivity<*>).hideProgress()
            }
        }
    }

    abstract fun initialize()
    abstract fun makeView(isLocaleUrdu: Boolean) : ComposeView
    open fun loader(): LiveData<Loading>? {
        return null
    }


    /**
     * Tells this [BottomSheetDialogFragment] to extract the arguments from [Bundle].
     *
     *
     * This method will be executed only if the [Bundle] argument was set during the instantiation
     * of this [BottomSheetDialogFragment].
     *
     * @param arguments [Bundle] that contains the arguments.
     */
    protected open fun extractArguments(arguments: Bundle){}

    private fun initDialog() {
        requireDialog().window?.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        requireDialog().window?.statusBarColor = requireContext().getColor(android.R.color.transparent)
    }
}
