package com.truckitin.core.appconstants

object AppConstants {
    /**
     * this is agreed upon value from BE which will be the lowest bid one can post
     */
    const val MIN_BID_VALUE = 5000.0
    /**
     * used to open location on google maps
     */
    const val GMAPs_URI = "http://maps.google.com/maps?q=loc:"
}
